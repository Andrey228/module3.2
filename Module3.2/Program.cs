﻿using System;
using System.Collections.Generic;

namespace Module3_2
{
    public static class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello world!");
        }
    }

    public class Task4
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            if (int.TryParse(input, out int gena) && gena>0)
            {
                result = int.Parse(input);
                return true;
            }
            else
            {
                result = 0;
                Console.WriteLine("Error. Input natural number");
                input = Console.ReadLine();
                return TryParseNaturalNumber(input, out result);
            }

        }

        public int[] GetFibonacciSequence(int n)
        {
            int[] fibonacci = new int[n];
            fibonacci[0] = 0;
            fibonacci[1] = 1;
            for(int i=2;i<n;i++)
                fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2];
            return fibonacci;
        }
    }

    public class Task5
    {
        public int ReverseNumber(int sourceNumber)
        {
            char tmp;
            string res = Convert.ToString(sourceNumber);
            char[] num = res.ToCharArray();
            char[] num1 = new char[num.Length];
            for(int i=0;i<num.Length;i++)
            {
                num1[num.Length-1 - i] = num[i];
            }
            string rez = new string(num1);
            int number = int.Parse(rez);
            return number;
        }
    }

    public class Task6
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            Random rand = new Random();
            if(size<=0)
            {
                return new int[0];
            }
            else
            {
                int[] array = new int[size];
                for(int i=0;i<size;i++)
                    array[i] = rand.Next(1,21);
                return array;
            }
            
        }

        public int[] UpdateElementToOppositeSign(int[] source)
        {
            for(int i=0;i<source.Length;i++)
                if (source[i] < 0 || source[i]>0)
                    source[i] = -source[i];
            return source;
        }
    }

    public class Task7
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
           if(size<=0)
                return new int[0];
            else
            {
                Random rand = new Random();
                int[] array = new int[size];
                for (int i = 0; i < size; i++)
                    array[i] = rand.Next(1, 21);
                return array;
            }     
        }

        public List<int> FindElementGreaterThenPrevious(int[] source)
        {
            List<int> elements = new List<int>();
            for (int i = 0; i < source.Length - 1; i++)
                if (source[i] < source[i + 1])
                    elements.Add(source[i + 1]);
            return elements;
        }
    }

    public class Task8
    {
        public int[,] FillArrayInSpiral(int size)
        {
            if (size <= 0)
            {
                return new int[0,0];
            }
            int[,] mas = new int[size, size];
            int i = 1, start = 0, side = size, x, y;
            while (i <= size * size)
            {
                x = start;
                y = start;
                for (int j = 0; j < side; j++)
                {
                    mas[x, y++] = i;
                    i++;
                }
                y--;
                for (int j = 0; j < side - 1; j++)
                {
                    mas[++x, y] = i;
                    i++;
                }
                y--;
                for (int j = 0; j < side - 1; j++)
                {
                    mas[x, y--] = i;
                    i++;
                }
                y++;
                for (int j = 0; j < side - 2; j++)
                {
                    mas[--x, y] = i;
                    i++;
                }
                start++;
                side -= 2;

            }
            return mas;
        }
    }
}
